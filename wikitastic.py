#!/usr/bin/env python3
#
#

import argparse, requests, json, os
from lxml import etree
from shutil import rmtree

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥
 
parser = argparse.ArgumentParser()
parser.add_argument("-w", "--wiki", help="wiki URL", type=str, required=True)
parser.add_argument("-c", "--category", help="category", type=str, required=True)
parser.add_argument("-o", "--output", help="out path", type=str, required=True)
args = parser.parse_args()
 
# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

def make_uri(title):
	URI = title.lower()
	for char in ['(', ')', ':']:
		URI = URI.replace(char, '')
	for char in [' - ', '/', ' ']:
		URI = URI.replace(char, '-')
	URI = URI + '.html'
	return URI


def make_output_dir(path):
	if os.path.exists(path):
		print(path)
		rmtree(path)
		os.makedirs(path + '/media')
	else:
		os.makedirs(path + '/media')


def write_page(filename, content):
	file = args.output + '/' + filename
	if not os.path.exists(file):
		with open(file, 'w') as f:
			f.write(content)
			f.close()
	else:
		with open(file, 'a') as f:
			f.write(content)
			f.close()


def make_request(action, options):
	request = args.wiki	+ '/api.php?action=' + action + '&format=json' + options
	r = requests.get(request)
	return r


def get_text(pageid):
	options = '&pageid=' + str(pageid) + '&prop=text&disablelimitreport=1' \
		+ '&disableeditsection=1&disabletoc=1&utf8=1'
	r = make_request('parse', options)
	response = json.loads(r.text)
	text = response['parse']['text']['*']
	return text


def find_values(id, json_repr):
	results = []
	
	def _decode_dict(a_dict):
		try: results.append(a_dict[id])
		except KeyError: pass
		return a_dict

	json.loads(json_repr, object_hook=_decode_dict)  # Return value ignored.
	return results


def get_file(href):
	file = args.output + '/media/' + href[6:]
	options = '&prop=imageinfo&titles=' + href[1:] + '&iiprop=url'
	r = make_request('query', options)
	url = find_values('url', r.text)[0]
	with open(file, "wb") as f:
		print("downloading " + file)
		r = requests.get(url)
		f.write(r.content)


def generate_html(title, body):
	head = '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" />' \
	+ '<title>' + title + '</title>' \
	+ '<link rel="stylesheet" href="style.css" /></head><body>' \

	tail = '</body></html>'

	return head + body + tail


def make_page(title, filename, text):
	html = generate_html(title, text)
	root = etree.XML(html)

	etree.strip_tags(root, 'span')		# remove span crap from mw

	links = list(root.iter("a"))
	for link in links:
		href = link.attrib['href']
		
		if href.startswith('/File:'):
			get_file(href)
			link.attrib['href'] = 'media/' + href[6:]
			img = link.find('img')
			img.attrib['src'] = link.attrib['href']
			try:
				del img.attrib['width']
				del img.attrib['height']
				del img.attrib['srcset']
			except KeyError:
				pass

	write_page(filename, etree.tostring(root, encoding='unicode'))


def make_index(pages):
	print('making index')

	body = '<h1>' + args.category + '</h1><ul>'

	for page in pages:
		body += '<li><a href="' + page[1] + '">' \
		+ page[0] + '</a></li>'

	body += '</ul>'

	html = generate_html(args.category, body)

	write_page('index.html', html)


def make_pages():
	pages = []
	print('Looking into ' + args.wiki + '/Category:' + args.category)
	opt = '&list=categorymembers&cmlimit=500&cmtitle=Category:' + args.category
	r = make_request('query', opt)
	data = json.loads(r.text)

	category_members = data['query']['categorymembers']

	print('Found ' + str(len(category_members)) + ' pages') 

	make_output_dir(args.output)

	for member in category_members:
		pageid = member['pageid']
		title = member['title']
		filename = make_uri(title)
		
		print(title, pageid)
		
		text = get_text(pageid)
		make_page(title, filename, text) 

		pages.append([title, filename])
	make_index(pages)

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥


make_pages()








